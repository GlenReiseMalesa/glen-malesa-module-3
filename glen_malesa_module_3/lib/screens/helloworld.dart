
// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';


class HelloWorld extends StatefulWidget {
  @override
  _HelloWorldState createState() => _HelloWorldState();
}

class _HelloWorldState extends State<HelloWorld> {

  @override
  Widget build(BuildContext context) {
 

    return Scaffold(
      appBar: AppBar(title: Text('HelloWorld')),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Text("Hello World"),
        ),
      ),
    );
  }
}
