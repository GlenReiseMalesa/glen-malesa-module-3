import 'package:flutter/material.dart';

class dashboard extends StatefulWidget {
  const dashboard({Key? key}) : super(key: key);

  @override
  _dashboardState createState() => _dashboardState();
}

class _dashboardState extends State<dashboard> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar:
          AppBar(title: const Text('Book An Appointment'), actions: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 15.0),
          child: GestureDetector(
            onTap: () {
              Navigator.pushReplacementNamed(context, '/profile');
            },
            child: const Icon(Icons.person_add_alt_1_outlined),
          ),
        ),
      ]),
      body: SingleChildScrollView(
        child: Container(),
      ),
            floatingActionButton: FloatingActionButton(
        // isExtended: true,
        child: Icon(Icons.add),
        backgroundColor: Colors.green,
        onPressed: () {
            Navigator.pushReplacementNamed(context, '/helloworld');
        },
      ),
    );
  }
}