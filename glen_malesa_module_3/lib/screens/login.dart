// ignore_for_file: prefer_function_declarations_over_variables, prefer_const_constructors

import '/utility/textfromdeco.dart';
import '/utility/reusable_widgets.dart';
import '/utility/validator.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  late String _email, _password;

  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();



  @override
  Widget build(BuildContext context) {


    var loginFunction = () async {
      _email = _emailController.text;
      _password = _passwordController.text;
      Navigator.pushReplacementNamed(context, '/dashboard');
    };

    //rerouting
    final register = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        SizedBox(
          height: 5.0,
        ),
        FlatButton(
            padding: EdgeInsets.only(left: 0.0),
            child:
                Text("Register", style: TextStyle(fontWeight: FontWeight.w300)),
            onPressed: () {
              Navigator.pushReplacementNamed(context, '/register');
            }),
      ],
    );

    return Scaffold(
      appBar: AppBar(
        title: const Text('Login'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(40.0),
          child: Form(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              // ignore: prefer_const_literals_to_create_immutables
              children: [
                SizedBox(
                  height: 15.0,
                ),
                Text("Email"),
                SizedBox(
                  height: 5.0,
                ),
                TextFormField(
                    controller: _emailController,
                    autofocus: false,
                    validator: (value) => validateEmail(value!),
                    decoration:
                        buildInputDecoration("Enter Your Email", Icons.email)),
                SizedBox(height: 20.0),
                Text("Password"),
                SizedBox(
                  height: 5.0,
                ),
                TextFormField(
                    controller: _passwordController,
                    autofocus: false,
                    validator: (value) =>
                        value!.isEmpty ? "Please enter your password" : null,
                    decoration: buildInputDecoration(
                        "Enter Your Password", Icons.password)),
                SizedBox(height: 20.0),
                longButtons("Login", loginFunction),
                SizedBox(height: 20.0),
                register
              ],
            ),
          ),
        ),
      ),
    );
  }
}
