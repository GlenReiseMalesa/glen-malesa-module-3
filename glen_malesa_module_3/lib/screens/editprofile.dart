
import '/utility/reusable_widgets.dart';
import '/utility/textfromdeco.dart';
import '/utility/validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EditProfile extends StatefulWidget {
  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  late String _fullName, _email, _password;
//getting text from the textform widget using controllers
  final _fullNameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {

// ignore: prefer_function_declarations_over_variables
    var EditProfileFunction = () async {
      _fullName = _fullNameController.text;
      _email = _emailController.text;
      _password = _passwordController.text;
      Navigator.pushReplacementNamed(context, '/profile');
       
    };


    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Profile'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(40.0),
          child: Form(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            // ignore: prefer_const_literals_to_create_immutables
            children: [
              SizedBox(
                height: 15.0,
              ),
              Text("FullName"),
              SizedBox(
                height: 5.0,
              ),
              TextFormField(
                controller: _fullNameController,
                autofocus: false,
                validator: (value) =>
                    value!.isEmpty ? "Please enter your fullname" : null,
                decoration: buildInputDecoration(
                    "Enter your fullname", Icons.person_off_outlined),
              ),
              SizedBox(
                height: 20.0,
              ),
              Text("Email"),
              SizedBox(
                height: 5.0,
              ),
              TextFormField(
                controller: _emailController,
                autofocus: false,
                validator: (value) => validateEmail(value!),
                decoration:
                    buildInputDecoration("Enter your email", Icons.email),
              ),
              SizedBox(
                height: 20.0,
              ),
              Text("Password"),
              SizedBox(
                height: 5.0,
              ),
              TextFormField(
                controller: _passwordController,
                autofocus: false,
                validator: (value) =>
                    value!.isEmpty ? "Please enter your password" : null,
                decoration:
                    buildInputDecoration("Enter your password", Icons.password),
              ),
              SizedBox(
                height: 20.0,
              ),
              longButtons("Update", EditProfileFunction),
              SizedBox(
                height: 20.0,
              ),

            ],
          )),
        ),
      ),
    );
  }
}
