import 'package:flutter/material.dart';
import 'package:glen_malesa_module_3/screens/editprofile.dart';
import 'package:glen_malesa_module_3/screens/helloworld.dart';

import 'screens/dashboard.dart';
import 'screens/login.dart';
import 'screens/profile.dart';
import 'screens/register.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'eDoctor',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Login(),
        routes: {
          //when you go to a certain route a certain intent event is triggered
          '/login': (context) => Login(),
          '/register': (context) => register(),
          '/dashboard': (context) => dashboard(),
          '/profile': (context) => profile(),
          '/editProfile': (context) => EditProfile(),
          '/helloworld': (context) => HelloWorld()
        });
  }
}
